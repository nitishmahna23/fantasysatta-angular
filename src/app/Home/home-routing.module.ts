import {NgModule} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';

import { HomeComponent } from './home.component';
import { Route } from '@angular/router/src/config';
import { LiveScoresComponent } from '../LiveScores/liveScores.component';

const routes: Routes=[
    {path: '',component:HomeComponent}
];

@NgModule({
        imports:[RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
    export class homeRoutingModule{ }
    export const routingComponent=[HomeComponent, LiveScoresComponent]
