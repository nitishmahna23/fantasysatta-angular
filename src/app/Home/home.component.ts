import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import { Router,  NavigationExtras,ActivatedRoute } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../Services/shared.service';
import { constantVariables } from '../Constants/constants';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'home-root',
  templateUrl: '../home/home.component.html',
  //styleUrls: ['./homess.component.css']
  providers:[DataService,constantVariables]
})
export class HomeComponent implements OnInit { 
  @ViewChild('username') el:ElementRef;
  statuslogin:any;
  focusin: boolean = true;
  loginForm: FormGroup;
  post:any;  
  emailAlert:string="Please fill email";
  passwordAlert:string="Please fill password";

  loginSuccessAlert="Login Successful";
  loginFailedAlert="Login Failed";
  loginFailedMessage="";

  loginAlert:string;
  loginError:boolean=false;
  loginFailed:boolean=false;
  loginSuccess:boolean=false;
  returnUrl: string;
  public imagesUrl;
  model: any = {};
  registerForm:FormGroup;

   
  registerFailed:boolean=false;
  registerSuccess:boolean=false;
  registerSuccessAlert="Register Successful";
  registerFailedAlert="Register Failed";
  registerFailedMessage="";
  registerSuccessMessage="";
  constructor(
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private authenticationservice:DataService,    
      public router: Router,
      private spinnerService: Ng4LoadingSpinnerService,
      private _constantVariable: constantVariables
    ) {
    this.loginForm = fb.group({
      'email' : [null, Validators.required],
      'password' : [null, Validators.required],
    });
    this.registerForm=fb.group({      
      'email' : ['', Validators.required],
      'username' : ['', Validators.required],
      'name' : ['', Validators.required],
      'password' : ['', Validators.required],
      'mobileNumber' : ['', Validators.required],
    });
  }
   ngOnInit() {
    this.imagesUrl = [
      'assets/images/007.jpg',
      'assets/images/006.jpg',
      'assets/images/005.jpg',
      ];
    //this.authenticationservice.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/index';
} 


  checkRegister(post){
    this.spinnerService.show();
    post.roleType= this._constantVariable.webApp;
    this.registerFailed=false;
    this.registerSuccess=false;
    this.authenticationservice.checkFromDb('user/register',post)
    .subscribe((result) => {
      //console.log(result);
      //console.log('In Success');
      this.registerSuccessMessage=result.message;
      this.registerSuccess=true;
      this.spinnerService.hide();

    }),(error)=>{
      var res=error.json();
      //console.log(res.message);
      this.registerFailedMessage=res.message;
      this.registerFailed=true;
      this.spinnerService.hide();
    }
  }
  

  checkLogin(post)
  {
    this.spinnerService.show();
      post.roleType= this._constantVariable.webApp;
      this.loginFailed=false;
      this.loginSuccess=false;
      //console.log(username+" "+password);
        //username='peter@klaven';password='cityslicka';
        this.authenticationservice.checkFromDb('user/login',post)
        .subscribe((result) => {
          //console.log(result.accessToken);
          sessionStorage.setItem('currentUser', JSON.stringify(result.accessToken));
          //var a=sessionStorage.getItem('currentUser');
          //console.log(a);
          
          this.loginSuccess=true;
          this.spinnerService.hide();

        }),(error)=>{
          var res=error.json();
          //console.log(res.message);
          this.loginFailed=true;
          this.loginFailedMessage=res.message;
          this.spinnerService.hide();
        }
    }
  }
