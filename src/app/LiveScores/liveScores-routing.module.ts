import {NgModule} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';

import { LiveScoresComponent } from './liveScores.component';

const routes: Routes=[
    {path: '',component:LiveScoresComponent}
];

@NgModule({
        imports:[RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
    export class liveScoresRoutingModule{ }
    export const routingComponent=[LiveScoresComponent]