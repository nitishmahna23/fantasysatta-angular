 import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import { Router,  NavigationExtras,ActivatedRoute } from '@angular/router';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
 import { DataService } from '../Services/shared.service';
 //import { constantVariables } from '../Constants/constants';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'liveScores-root',
  templateUrl: '../liveScores/liveScores.component.html',
  // styleUrls: ['./homess.component.css']
   providers:[DataService]//,constantVariables]
})
export class LiveScoresComponent implements OnInit{
  matches: string;
  constructor(private _authenticationservice:DataService,
    //private _constantVariable: constantVariables,
    private spinnerService: Ng4LoadingSpinnerService) {   
  }
  ngOnInit() {    
    this.getMatches('matches/SB8z2t9TMCgRrOzFwAFhSHbMxVd2');
  }
  getMatches(cricApiUrl:string){
    this.spinnerService.show();
    this._authenticationservice.getMaches(cricApiUrl)
    .subscribe((result) => {
     
     console.log(result.matches[0]);
     this.matches=result.matches[0];
      this.spinnerService.hide();

    }),(error)=>{
      var res=error.json();
      console.log(res.message);
      // this.registerFailedMessage=res.message;
      // this.registerFailed=true;
      this.spinnerService.hide();
    }
  }
  }
