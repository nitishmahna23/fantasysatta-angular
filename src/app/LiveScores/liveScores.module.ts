
import { NgModule } from '@angular/core';
import { routingComponent, liveScoresRoutingModule } from './liveScores-routing.module';
// import { TabsModule } from 'ngx-bootstrap';
 import { CommonModule }   from '@angular/common';
// import { ReactiveFormsModule } from '@angular/forms';
// import { FormsModule } from "@angular/forms";

// //import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { SliderModule } from 'angular-image-slider';

 import { HttpModule } from '@angular/http';
// //import { AppComponent } from './app.component';
// import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';




@NgModule({
    declarations: [
      routingComponent
    ],
    imports: [
        liveScoresRoutingModule,
      //TabsModule,
      CommonModule,
      //ReactiveFormsModule
      //,SliderModule
      //,BrowserAnimationsModule
     // ,FormsModule,
      HttpModule,
      //Ng4LoadingSpinnerModule.forRoot()
    ],
    //bootstrap: [AppComponent]
  })
  export class liveScoresModule { }