import { Http, Response, Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Observable';
import { constantVariables } from '../Constants/constants';
import { Injectable, Compiler } from '@angular/core';
//import { RequestOptions } from '@angular/http/src/base_request_options';

@Injectable()
export class DataService { 
  private actionUrl: string;
  private cricApiUrl:string;

  constructor(private _http: Http, private _constantVariable: constantVariables) {
    this.actionUrl =  this._constantVariable.apiUrl;
    this.cricApiUrl=this._constantVariable.cricApiUrl;
}

// public GetAll = (): Observable<any> => {
//     return this._http.get(this.actionUrl)
//         .map((response: Response) => <any>response.json())
//         .do(x => console.log(x));


checkFromDb(_Url: string, data: any) {
let apiUrl= this.actionUrl+_Url;
//console.log(apiUrl);
let headers=new Headers({
        'Content-Type':'application/json'
});
let options= new RequestOptions({
    headers:headers
});
return this._http.post(apiUrl,data,options)
    .map((res: Response) => {
        let result = res.json();
        //console.log(result);
        if(result) {
            return result;
        }
    })  
}
getMaches(_Url: string){
    let apiUrl= this.cricApiUrl+_Url;
let headers=new Headers({
        'Content-Type':'application/json'
});
let options= new RequestOptions({
    headers:headers
});

return this._http.get(apiUrl, options)
    .map((res: Response) => {
        let result = res.json();
        //console.log(result);
        if(result) {
            return result;
        }
    })  
}  


}