import { RouterModule, Routes } from '@angular/router';

//import { HomeComponent } from '../app/Home/home.component';
import { ModuleWithProviders } from '@angular/compiler/src/core';

export const appRoutes: Routes = [
    { path: '', loadChildren: './Home/home.module#homeModule' },
    { path: 'home', loadChildren: './Home/home.module#homeModule' },
    { path: 'liveScores', loadChildren: './LiveScores/liveScores.module#liveScoresModule' },
   
   // { path: '**', component: PageNotFoundComponent }
  ];

  export const routing: ModuleWithProviders= RouterModule.forRoot(appRoutes,{useHash:true});